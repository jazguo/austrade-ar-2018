/**
 * Created by Jason Guo
 * Copyright 2012 - 2017 XiNG Digital Pty Ltd
 * info@xing.net.au
 */
function randString(length)
{
    if (!length) {
        length = 12;
    }
    var string = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (var i=0; i < length; i++) {
        string += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return string;
}

jQuery(document).ready(function($) {

  // Add floating scroll-to-top button

  $(window).scroll(function(){
    if ($(document).scrollTop() >= 50) {
      $("#back-to-top").show();
    } else {
      $("#back-to-top").hide();
    }
  });

  $("#back-to-top").click(function() {
    $("html, body").animate({ scrollTop: $('#content').offset().top }, 500);
  });

  // Sidebar table of contents stuff

  $("#sidebar-first > .block > ul.menu > li.expanded > a ").click(function(event) {
    if ($(this).next().is(':hidden') == true) {
      event.preventDefault();
      $("#sidebar-first > .block > ul.menu > li.expanded > ul:visible").slideUp('fast');
      $("#sidebar-first > .block > ul.menu > li.expanded").removeClass('clickable');
      $(this).next().slideDown('slow');
      $(this).addClass('clickable');
    }
  });

  $("#sidebar-first ul.menu li.expanded > ul a.active").closest("ul").show();
  $("#sidebar-first ul.menu a.active").next().show();
  $("#sidebar-first ul.menu li.expanded > ul a.active").closest("ul").parent().closest("ul").show();

  $("#sidebar-first ul.menu li.expanded > ul a.active").closest("ul").parents().addClass('active-child');

  // Smooth scrolling for homepage anchor links wrapped in class = block-anchor-link

  $('body .back-to-top a[href*=#]:not([href=#])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
        || location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 110
        }, 500);
        return false;
      }
    }
  });

  //
  // Add header IDs to cells in complex tables.
  // WCAG 2.0 H43 - https://www.w3.org/TR/WCAG20-TECHS/H43.html
  //

  $("table thead th").each(function(index) {
    $(this).attr("id",  randString(12));
  });

  var thLocator = [], colCount = 0, rowCount = 0;
  $("table").find('thead tr').each(function () {
    $(this).find('th').each(function () {
      for (var i = 0; i < this.colSpan; i++) {
        if (this.rowSpan <= 1) {
          $(this).attr('scope', 'col');
          var objectToPush = new Object();
          objectToPush['rowIndex'] = rowCount;
          objectToPush['colIndex'] = colCount;
          objectToPush['id'] = $(this).attr('id');
          thLocator.push(objectToPush);
          colCount++;
        }
        // For debug only
        // else {
        //  alert('Header cell(s) may contain rowspan. Please check output!');
        // }
      }
    });	// EOF each th
    colCount = 0;
    rowCount++;
  }); // EOF each thead tr

  $("table").find('tbody th').each(function () {
    $(this).attr('scope', 'row');
  });

  $("table").find('tbody tr').each(function () {
    $(this).find('td').each(function () {
      var tdColIndex = $(this).index();
      var headers = '';
      for (var k = 0; k < thLocator.length; k++) {
        $.each(thLocator[k], function(index, value) {
          if (thLocator[k]['colIndex'] == tdColIndex) {
            if (headers.indexOf(thLocator[k]['id']) < 0 ) {
              headers = headers + ' ' + thLocator[k]['id'];
            }
          }
        });
      }
      $(this).attr('headers', headers.trim());
    });
  });

  // Add header attributes to other <th> cells in <thead>
  $("table").find('thead tr:not(:first)').each(function () {
    $(this).find('th').each(function () {
      console.log($(this));
      var thColIndex = $(this).index();
      var headers = '';
      for (var k = 0; k < thLocator.length; k++) {
        $.each(thLocator[k], function(index, value) {
          if (thLocator[k]['colIndex'] == thColIndex && thLocator[k]['rowIndex'] == 0) {
            if (headers.indexOf(thLocator[k]['id']) < 0 ) {
              headers = headers + ' ' + thLocator[k]['id'];
            }
          }
        });
      }
      $(this).attr('headers', headers.trim());
    });
  });
});
